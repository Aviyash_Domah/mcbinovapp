import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GoogleMaps, GoogleMapsEvent, GoogleMapsAnimation } from '@ionic-native/google-maps';
var HomePage = /** @class */ (function () {
    function HomePage() {
        this.mapReady = false;
        this.loadMap();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.loadMap();
    };
    HomePage.prototype.loadMap = function () {
        var _this = this;
        // Create a map after the view is loaded.
        // (platform is already ready in app.component.ts)
        this.map = GoogleMaps.create('map_canvas', {
            camera: {
                target: {
                    lat: 43.0741704,
                    lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            }
        });
        // Wait the maps plugin is ready until the MAP_READY event
        this.map.one(GoogleMapsEvent.MAP_READY).then(function () {
            _this.mapReady = true;
        });
    };
    HomePage.prototype.onButtonClick = function () {
        var _this = this;
        if (!this.mapReady) {
            alert('map is not ready yet. Please try again.');
            return;
        }
        this.map.clear();
        // Get the location of you
        this.map.getMyLocation()
            .then(function (location) {
            console.log(JSON.stringify(location, null, 2));
            // Move the map camera to the location with animation
            return _this.map.animateCamera({
                target: location.latLng,
                zoom: 17,
                tilt: 30
            }).then(function () {
                // add a marker
                return _this.map.addMarker({
                    title: '@ionic-native/google-maps plugin!',
                    snippet: 'This plugin is awesome!',
                    position: location.latLng,
                    animation: GoogleMapsAnimation.BOUNCE
                });
            });
        }).then(function (marker) {
            // show the infoWindow
            marker.showInfoWindow();
            // If clicked it, display the alert
            marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(function () {
                alert('clicked!');
            });
        });
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map