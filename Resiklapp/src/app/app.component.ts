import {
  Component
} from '@angular/core';

import {
  Platform,
  NavController
} from '@ionic/angular';
import {
  SplashScreen
} from '@ionic-native/splash-screen/ngx';
import {
  StatusBar
} from '@ionic-native/status-bar/ngx';
import {
  LoginPage
} from './login/login.page';
import {
  Router
} from '@angular/router';
import {
  Storage
} from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [{
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'My Account',
      url: '/myaccount',
      icon: 'person'
    },
    {
      title: 'Collection Request',
      url: '/request',
      icon: 'document'
    }
  ];

  public isUserLoggedIn: Boolean;
  public user: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage
  ) {

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {


      this.isUserLoggedIn = false;

      // Or to get a key/value pair
      this.storage.get('username').then((val) => {
        console.log('Your username is', val);
        this.user = "You are logged in as " + val;
        if (val == null || val == "") { // if not login
          this.router.navigateByUrl('/');

        } else { // if login
          this.user = "You are logged in as " + val;
          this.router.navigateByUrl('/home');
        }
      }).catch(err => {
        console.log(err);
        this.router.navigateByUrl('/');
      });

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}