import { Component } from '@angular/core';
import { GoogleMaps,  GoogleMap,  GoogleMapsEvent,  Marker,  GoogleMapsAnimation,  MyLocation, Environment, LatLng } from '@ionic-native/google-maps';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  mapReady: boolean = false;
  map: GoogleMap;
  username: string;

  constructor(
    private storage:Storage,
    private router: Router,
    private androidPermissions: AndroidPermissions,
    private menuCtrl: MenuController
    ) {
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeEnable(true);
      this.loadMap();
      // Or to get a key/value pair
      this.storage.get('username').then((val) => {
        console.log('Your username is', val);
        this.username = val;
      }).catch(err => {
        console.log(err);
        this.username = err;
      });
  }

  public loadMap() {
    // Create a map after the view is loaded.
    // (platform is already ready in app.component.ts)
      // This code is necessary for browser
      /*Environment.setEnv({
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyD4Rf5Su3WKmQSdkSkFCZS8YKWeT4xpGGE',
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyD4Rf5Su3WKmQSdkSkFCZS8YKWeT4xpGGE'
      });*/
  
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 43.0741704,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    });

    // Wait the maps plugin is ready until the MAP_READY event
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.mapReady = true;

      this.onButtonClick();
    });

    
  }

  public onButtonClick() {
    //this.displayPickUpPoints();
    if (!this.mapReady) {
      alert('map is not ready yet. Please try again.');
      return;
    }
    //this.map.clear();
    this.map.setMyLocationButtonEnabled(true);
    this.map.setMyLocationEnabled(true);
    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    .then(() => {
      // Get the location of you
    this.map.getMyLocation()
    .then((location: MyLocation) => {
      //console.log(JSON.stringify(location, null ,2));

      // Move the map camera to the location with animation
      return this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        tilt: 30
      }).then(() => {
        // add a marker
        return this.map.addMarker({
          title: this.username,
          //snippet: 'This plugin is awesome!',
          position: location.latLng,
          animation: GoogleMapsAnimation.BOUNCE        
        });
      })
    }).then((marker: Marker) => {
      // show the infoWindow
      marker.showInfoWindow();
      this.displayPickUpPoints();
      // If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        alert('clicked!');
      });
    });
  }).catch((err) => {
    alert(JSON.stringify(err));
});
    /*
    // Get the location of you
    this.map.getMyLocation()
      .then((location: MyLocation) => {
        console.log(JSON.stringify(location, null ,2));

        // Move the map camera to the location with animation
        return this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        }).then(() => {
          // add a marker
          return this.map.addMarker({
            title: '@ionic-native/google-maps plugin!',
            snippet: 'This plugin is awesome!',
            position: location.latLng,
            animation: GoogleMapsAnimation.BOUNCE,
            icon: {
              url: "https://aerowsocialclub.000webhostapp.com/logo.jpg",
              size: {
                width: 36,
                height: 36
              }
            }
          });
        })
      }).then((marker: Marker) => {
        // show the infoWindow
        marker.showInfoWindow();

        // If clicked it, display the alert
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          alert('clicked!');
        });
      });
      */
  }

  public logout(event) {
    var loggedOut = confirm("Do you want to logout?");
    console.log("logout");
    this.storage.clear();
    this.router.navigateByUrl('/');
  }

  public displayPickUpPoints() {
    
    this.map.addMarker({
        position: new LatLng(-20.009214, 57.556224),
        animation: GoogleMapsAnimation.BOUNCE,
        icon: {
          url: "https://aerowsocialclub.000webhostapp.com/pick.png",
          size: {
            width: 36,
            height: 36
          }
        }
      });

      this.map.addMarker({
        position: new LatLng(-20.002769, 57.558133),
        animation: GoogleMapsAnimation.BOUNCE,
        icon: {
          url: "https://aerowsocialclub.000webhostapp.com/pick.png",
          size: {
            width: 36,
            height: 36
          }
        }
      });

      this.map.addMarker({
        position: new LatLng(-20.006317, 57.558927),
        animation: GoogleMapsAnimation.BOUNCE,
        icon: {
          url: "https://aerowsocialclub.000webhostapp.com/pick.png",
          size: {
            width: 36,
            height: 36
          }
        }
      });

      this.map.addMarker({
        position: new LatLng(-20.003222, 57.552490),
        animation: GoogleMapsAnimation.BOUNCE,
        icon: {
          url: "https://aerowsocialclub.000webhostapp.com/pick.png",
          size: {
            width: 36,
            height: 36
          }
        },
      });

      this.map.addMarker({
        position: new LatLng(-20.002446, 57.553659),
        animation: GoogleMapsAnimation.BOUNCE,
        icon: {
          url: "https://aerowsocialclub.000webhostapp.com/pick.png",
          size: {
            width: 36,
            height: 36
          }
        }
      });
  }

}
