import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  Storage
} from '@ionic/storage';

import { MenuController } from '@ionic/angular';


import { LoadingService } from '../service/loading.service';
//import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public username: string;
  public password: string;
  constructor(
    private router: Router,
    private storage: Storage,
    private loading: LoadingService,
    private menuCtrl: MenuController
    //private iab: InAppBrowser
  ) {}

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }

  public Login(event) {
    
    this.loading.present();
    //this.iab.create("https://htmlcolorcodes.com/","_blank").show();
    if (this.username == null) {
      alert("Username is required");
      this.loading.dismiss();
      return;
    }

    if (this.password == null) {
      alert("Password is required");
      this.loading.dismiss();
      return;
    }

    if (this.username != null && this.password != null) {
      //login user
      // set a key/value
      this.storage.set('username', this.username);
      this.loading.dismiss();
      this.router.navigateByUrl('/home');
    }
  }

  public SignUp(event) {
    this.loading.present();
    this.router.navigateByUrl('/signup');
    this.loading.dismiss();
  }
}