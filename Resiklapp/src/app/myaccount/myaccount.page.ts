import {
  Component,
  OnInit
} from '@angular/core';
import {
  MenuController
} from '@ionic/angular';
import {
  Storage
} from '@ionic/storage';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {

  constructor(
    private menuCtrl: MenuController,
    private storage: Storage,
    private router: Router
  ) {}

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
  }

  public logout(event) {
    var loggedOut = confirm("Do you want to logout?");
    if (loggedOut) {
      console.log("logout");
      this.storage.clear();
      this.router.navigateByUrl('/');
    }
  }

}