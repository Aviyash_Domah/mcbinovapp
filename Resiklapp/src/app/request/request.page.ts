import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {

  constructor(
    private storage:Storage,
    private router: Router
    ) { }

  ngOnInit() {
  }

  public logout(event) {
    var loggedOut = confirm("Do you want to logout?");
    console.log("logout");
    this.storage.clear();
    this.router.navigateByUrl('/');
  }

}
