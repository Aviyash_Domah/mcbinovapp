import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
 
  public username:string;
  public password: string;
  public cpassword: string;
  constructor(private menuCtrl: MenuController) { }

  ngOnInit() {
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
  }

  public SignOut() {

    if (this.username == null) {
      alert("Username is required");
      return;
    }

    if (this.password == null) {
      alert("Password is required");
      return;
    }

    if (this.cpassword == null) {
      alert("Confirm Password is required");
      return;
    }

    if (this.username != null && this.password != null && this.cpassword != null) {

      if(this.password === this.cpassword) {
        //register user
      } else {
        alert("Password should match Confirm Password");
      }
      
    }

  }

}
