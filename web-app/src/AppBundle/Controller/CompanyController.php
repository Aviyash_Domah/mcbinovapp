<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\RegisterCompanyForm;
use AppBundle\Form\RegisterDriverForm;
use AppBundle\Form\CompanyRewardForm;
use AppBundle\Form\CompanyScheduleForm;
use AppBundle\Form\ClientRewardForm;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Users as User;
use AppBundle\Entity\CompanyRewards as CompanyRewards;
use AppBundle\Entity\ScheduleCollection as CompanySchedule;
use AppBundle\Entity\ClientRewards;

class CompanyController extends Controller
{
    /**
     * @Route("/company/dashboard", name="company_dashboard")
     */
    public function CompanyDashboardAction()
    {
        return new Response(
            '<html><body>Hello</body></html>'
        );
    }

     /**
     * @Route("/register/companyrrr", name="register_company")
     */
    public function RegisterCompanyAction(Request $request)
    {
        $form = $this->createForm(RegisterCompanyForm::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // var_dump($form->getData());die;
            
            $companyData = $form->getData();
            $company = new User();

            $company->setUsername($companyData['username']);
            $company->setPassword($companyData['password']);
            $company->setName($companyData['name']);
            $company->setPhone($companyData['phone']);
            $company->setMobile($companyData['mobile']);
            $company->setEmail($companyData['email']);
            $company->setAddress($companyData['address']);
            $company->setLocation($companyData['location']);
            $company->setRole('Company');
            $company->setBrn($companyData['brn']);
            $company->setLogo($companyData['logo']);
          


            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('company_dashboard');
        }

        
        return $this->render('register/company.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/register/driver", name="register_driver")
     */
    public function RegisterDriverAction(Request $request)
    {
        $form = $this->createForm(RegisterDriverForm::class);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // var_dump($form->getData());die;
                
                $driverData = $form->getData();
                $driver = new User();

                $driver->setUsername($driverData['username']);
                $driver->setPassword($driverData['password']);
                $driver->setName($driverData['name']);
                $driver->setPhone($driverData['phone']);
                $driver->setMobile($driverData['mobile']);
                $driver->setEmail($driverData['email']);
                $driver->setRole('Driver');
              
              


                $em = $this->getDoctrine()->getManager();
                $em->persist($driver);
                $em->flush();

                return $this->redirectToRoute('company_dashboard');
            }

            
            return $this->render('register/driver.html.twig', [
                'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/company/reward/{id}", name="company_reward")
     */
    public function CompanyRewardAction(Request $request, $id)
    {
        $form = $this->createForm(CompanyRewardForm::class);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // var_dump($form->getData());die;
                
                $rewardData = $form->getData();

                $company = $this->getDoctrine()->getRepository('AppBundle:Users')->find($id);

                $reward = new CompanyRewards();

                $reward->setCompany($company);
                $reward->setMaterial($rewardData['material']->getId());
                $reward->setReward($rewardData['reward']);
                $reward->setAmount($rewardData['amount']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($reward);
                $em->flush();

                return $this->redirectToRoute('company_dashboard');
            }

            
            return $this->render('company/reward.html.twig', [
                'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/company/schedule/{id}", name="company_schedule")
     */
    public function CompanyScheduleAction(Request $request, $id)
    {
        $form = $this->createForm(CompanyScheduleForm::class);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                
                $scheduleData = $form->getData();

                $company = $this->getDoctrine()->getRepository('AppBundle:Users')->find($id);

                $schedule = new CompanySchedule();

                $schedule->setCompany($company);
                $schedule->setMaterial($scheduleData['material']->getId());
                $schedule->setDate($scheduleData['date']);
                $schedule->setTime($scheduleData['time']);
                $schedule->setAddress($scheduleData['address']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($schedule);
                $em->flush();

                return $this->redirectToRoute('company_dashboard');
            }

            
            return $this->render('company/schedule.html.twig', [
                'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/company/client_reward/{id}/{user_id}", name="company_client_reward")
     */
    public function ClientRewardAction(Request $request, $id, $user_id)
    {
        $form = $this->createForm(ClientRewardForm::class);

        $form->handleRequest($request);
        if(!$form->isSubmitted())
        {
            $client_data = $this->getDoctrine()->getRepository('AppBundle:ClientRewards')->findOneby(array('id' => $user_id, "company" => $id));
            $client_name = $this->getDoctrine()->getRepository('AppBundle:Users')->find($client_data->getClient());
            $material = $this->getDoctrine()->getRepository('AppBundle:Materials')->find($client_data->getMaterial());

        }

        if ($form->isSubmitted() && $form->isValid()) {
            
            $reward_update = $form->getData();
            $client_reward = $this->getDoctrine()->getRepository('AppBundle:ClientRewards')->findOneby(array('id' => $user_id, "company" => $id));
            $company_reward = $this->getDoctrine()->getRepository('AppBundle:CompanyRewards')->findOneby(array('id' => $id));
            $reward = ($reward_update['amount']/$company_reward->getAmount()) * $company_reward->getReward();

            $client_reward->setReward($reward);

            $em = $this->getDoctrine()->getManager();
            $em->persist($client_reward);
            $em->flush();

            return $this->redirectToRoute('company_dashboard');
        }

        
        return $this->render('company/client.html.twig', [
            'form' => $form->createView(),
            'client_name' => $client_name,
            'client_material' => $material
        ]);

    }

}