<?php
// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\RegisterCompanyForm;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Company as Company;

class LuckyController extends Controller
{
    /**
     * @Route("/lucky/number", name="stupid_but_beautiful_rims")
     */
    public function numberAction()
    {
        $number = random_int(0, 40);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }

     /**
     * @Route("/register/company", name="admin_genus_new")
     */
    public function RegisterCompanyAction(Request $request)
    {
        $form = $this->createForm(RegisterCompanyForm::class);

         $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // var_dump($form->getData());die;
            
            $companyData= $form->getData();
            $company=new Company();

            $company->setName($companyData['name']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('stupid_but_beautiful_rims');
        }

        
        return $this->render('register/company.html.twig', [
            'form' => $form->createView()
        ]);

    }
}