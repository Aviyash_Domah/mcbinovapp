<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientRewards
 *
 * @ORM\Table(name="client_rewards")
 * @ORM\Entity
 */
class ClientRewards
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;
    
    /**
     * @ORM\Column(name="client_id", type="text", length=255, nullable=false)
     */
    private $client;
	
    /**
     * @ORM\Column(name="company_id", type="text", length=255, nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(name="material_id", type="text", length=255, nullable=false)
     */
    private $material;

    /**
     * @ORM\Column(name="amount", type="float", nullable=false)
     */
    private $amount;

    /**
     * @ORM\Column(name="reward", type="integer", length=255, nullable=false)
     */
    private $reward;



    /**
     * Set client
     *
     * @param \AppBundle\Entity\Users $client
     *
     * @return CompanyRewards
     */
    public function setClient(\AppBundle\Entity\Users $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Users
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * Set company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return CompanyRewards
     */
    public function setCompany(\AppBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set material
     *
     * @param \AppBundle\Entity\Material $material
     *
     * @return CompanyRewards
     */
    public function setMaterial(\AppBundle\Entity\Material $material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return \AppBundle\Entity\Material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set reward
     *
     * @param integer $reward
     *
     * @return CompanyRewards
     */
    public function setReward($reward)
    {
        $this->reward = $reward;

        return $this;
    }

    /**
     * Get reward
     *
     * @return integer
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return CompanyRewards
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
