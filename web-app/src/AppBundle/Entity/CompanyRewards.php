<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyRewards
 *
 * @ORM\Table(name="company_rewards")
 * @ORM\Entity
 */
class CompanyRewards
{

    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
     */
    private $id;
	
    /**
     * @ORM\Column(name="company_id", type="text", length=255, nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(name="material_id", type="text", length=255, nullable=false)
     */
    private $material;

    /**
     * @ORM\Column(name="amount", type="float", nullable=false)
     */
    private $amount;

    /**
     * @ORM\Column(name="reward", type="integer", length=255, nullable=false)
     */
    private $reward;


    /**
     * Set company
     *
     * @param \AppBundle\Entity\Users $company
     *
     * @return CompanyRewards
     */
    public function setCompany(\AppBundle\Entity\Users $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set material
     *
     * @param string $material
     *
     * @return CompanyRewards
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string $material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set reward
     *
     * @param integer $reward
     *
     * @return CompanyRewards
     */
    public function setReward($reward)
    {
        $this->reward = $reward;

        return $this;
    }

    /**
     * Get reward
     *
     * @return integer
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return CompanyRewards
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
