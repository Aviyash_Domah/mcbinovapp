<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduleCollection - by companies
 *
 * @ORM\Table(name="schedule_collection")
 * @ORM\Entity
 */
class ScheduleCollection
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="company_id", type="text", length=255, nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(name="material_id", type="text", length=255, nullable=false)
     */
    private $material;

    /**
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(name="time", type="time", nullable=false)
     */
    private $time;

    /**
     * @ORM\Column(name="address", type="text", length=255, nullable=false)
     */
    private $address;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param \AppBundle\Entity\Users $company
     *
     * @return company
     */
    public function setCompany(\AppBundle\Entity\Users $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set material
     *
     * @param string $material
     *
     * @return CompanySchedule
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string $material
     */
    public function getMaterial()
    {
        return $this->material;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ScheduleCollection
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /*** Get date
    *
    * @return \DateTime
    */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ScheduleCollection
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /*** Get time
    *
    * @return \DateTime
    */
    public function getTime()
    {
        return $this->time;
    }

     public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
}
