<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ClientRewardForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
    		->add(
				"amount",
				NumberType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank()
		            ),
		            "label" => "amount",
		            "label_attr" => array(
		                "for" => "amount",
		            )
            	)
            )
            ->add(
				"save",
				SubmitType::class,
					array(
			            "label" => "Update Reward"
	            	)
            )	
			;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}

?>