<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Materials;

class CompanyScheduleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
            ->add(
                "material",
                EntityType::class,
                array(
                    "class" => Materials::class,
                    "constraints" => array(
                        new Constraints\NotBlank()
                    ),
                    "label" => "material",
                    "label_attr" => array(
                        "for" => "material",
                    )
                )
            )
            ->add(
				"date",
				DateType::class,
				array(
		            "label" => "date",
		            "label_attr" => array(
		            "for" => "date",
		            )
            	)
            )
            ->add('time', 
                TimeType::class, 
                array(
                    'input'  => 'datetime',
                    'widget' => 'choice',
                )
            )
            ->add(
                "address",
                TextType::class,
                array(
                    "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
                    ),
                    "label" => "address",
                    "label_attr" => array(
                        "for" => "address"
                    )
                )
            )
            ->add(
                "save",
                SubmitType::class,
                array(
                    "label" => "Submit"
                )
            )
			;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}

?>