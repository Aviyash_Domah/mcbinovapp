<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterCompanyForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
            ->add(
				"username",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
                        //new Constraints\Callback(
                            //array($this, "verifyExistingName")
                        //),
		            ),
		            "label" => "username",
		            "label_attr" => array(
		                "for" => "username",
		            )
            	)
            )
			->add(
				"password",
				PasswordType::class,
				array(
		            "constraints" => array(
		                
                        new Constraints\Length(array("max" => 255))
		            ),
		            "label" => "password",
		            "label_attr" => array(
		                "for" => "password",
		            ),
                    "required" => false,
            	)
			)
			->add(
				"password_confirm",
				PasswordType::class,
				array(
		            "label" => "confirmation.password",
		            "label_attr" => array(
		                "for" => "password_confirmation",
		            ),
                    "required" => false,
            	)
			)
			->add(
				"name",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
                        //new Constraints\Callback(
                            //array($this, "verifyExistingName")
                        //),
		            ),
		            "label" => "name",
		            "label_attr" => array(
		                "for" => "name",
		            )
            	)
            )
			->add(
				"phone",
				TextType::class,
				array(
                    "constraints" => array(
                        new Constraints\Regex(
                            array(
                                'pattern' =>'/^([- +().0-9]){4,20}$/',
                                'match'   =>true,
                                'message'=>'error.error_phone',
                            )
                        )
                    ),
                    "label" => "phone",
                    "label_attr" => array(
                        "for" => "phone",
                    ),
                    "required" => false

                )
			)
			->add(
				"mobile",
				TextType::class,
				array(
                    "constraints" => array(
                        new Constraints\Regex(
                            array(
                                'pattern' =>'/^([- +().0-9]){4,20}$/',
                                'match'   =>true,
                                'message'=>'error.error_phone',
                            )
                        )
                    ),
                    "label" => "mobile",
                    "label_attr" => array(
                        "for" => "mobile",
                    ),
                    "required" => false

                )
			)
			->add(
				"email",
				EmailType::class,
				array(
		            "constraints" => array(
		                new Constraints\NotBlank(),
		                new Constraints\Email(),
                        new Constraints\Length(array("max" => 255)),
                        
		            ),
		            "label" => "mail",
		            "label_attr" => array(
		                "for" => "email",
		            ),
				)
            )
			->add(
				"address",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
		            ),
		            "label" => "address",
		            "label_attr" => array(
		                "for" => "address",
		            )
            	)
            )
			->add(
				"location",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
		            ),
		            "label" => "location",
		            "label_attr" => array(
		                "for" => "location",
		            )
            	)
            )
			
			->add(
				"brn",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
		            ),
		            "label" => "brn",
		            "label_attr" => array(
		                "for" => "brn",
		            )
            	)
            )
			->add(
				"logo",
				TextType::class,
				array(
		            "constraints" => array(
                        new Constraints\NotBlank(),
                        new Constraints\Length(array("max" => 255)),
		            ),
		            "label" => "logo",
		            "label_attr" => array(
		                "for" => "logo",
		            )
            	)
            )
			->add(
			"save",
			SubmitType::class,
				array(
		            "label" => "Submit"
            	)
            )			
			;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}

?>